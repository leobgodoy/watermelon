# watermelon

Olá, esse é um tutorial de como realizar o funcionamento do teste para a oferta da Watermelon, usando o terminal, presume-se que quem está executando teste possua em seu computador Angular 2, MongoDB, pymongo e Flask:

1º Clonar o repositório; 

2º Executar o funcionamento do MongoDB utilizando o comando "service mongod start" com o auxilio do comando "sudo";

3º Ativar o funcionamento do flask usando o comando "source flask-env/bin/activate";

4º Ir até a pasta "flask-env" e realizar o comando "python index.py", o backend do aplicativo estará acionado e os registros estarão disponíveis no db "teste_db" (isso pode ser verificado realizando a conferência em uma janela separada, usando os comandos "mongo", "use teste_db" e "db.catalogos.find({})"). Importante deixar essa janela intocada para o funcionamento ideal do teste;

5º Abrir um novo terminal e ir até a pasta "front-end-site";

6º Executar o comando "ng serve", após a execução do comando deixar a janela intocada para o funcionamento ideal do teste;

7º Ir no navegador de sua preferência e entrar na URL "http://localhost:4200/";

8º No topo da pagina você deve preencher os campos "nome", "e-mail" e "telefone" para poder habilitar o botão "enviar";

9º Ao clicar o botão "enviar", a pagina recarrega e os dados preenchidos são abastecidos nos cards localizados abaixo do header;

