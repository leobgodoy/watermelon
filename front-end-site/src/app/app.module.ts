import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { CatalogoService } from './catalogo/catalogo.service'

import { AppComponent } from './app.component';
import { CatalogoComponent } from './catalogo/catalogo.component';
import { InputNewDataComponent } from './input-new-data/input-new-data.component';

@NgModule({
  declarations: [
    AppComponent,
    CatalogoComponent,
    InputNewDataComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [CatalogoService, HttpClientModule, FormsModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
