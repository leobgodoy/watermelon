import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CatalogoService {

	private baseApiPath = "http://127.0.0.1:5000";

  	constructor(private http: HttpClient) { }

  	getCatalogo(){
  		return this.http.get(this.baseApiPath + '/get_records');
  	}

  	postDados(form){
  		return this.http.post(this.baseApiPath + '/insert_record', form.value, {responseType: 'text'}
  		).pipe(map(res => res)).subscribe(data => console.log(data));
  	}
}
