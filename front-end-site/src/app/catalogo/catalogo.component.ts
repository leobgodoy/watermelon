import { Component, OnInit } from '@angular/core';

import { CatalogoService } from './catalogo.service';

@Component({
  selector: 'app-catalogo',
  templateUrl: './catalogo.component.html',
  styleUrls: ['./catalogo.component.css']
})
export class CatalogoComponent implements OnInit {

	contatos: string[];

	constructor(
		public catalogoService: CatalogoService 
	) {	}

	ngOnInit() {
		this.catalogoService.getCatalogo().subscribe(
    	data => {
      	this.contatos = (data as any);
    	},
      error => {
        console.log("Error at getCatalogo()");
      }
  	)
	}
}
