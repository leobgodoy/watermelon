import { Component, OnInit } from '@angular/core';

import { CatalogoService } from '../catalogo/catalogo.service';

@Component({
  selector: 'app-input-new-data',
  templateUrl: './input-new-data.component.html',
  styleUrls: ['./input-new-data.component.css']
})
export class InputNewDataComponent implements OnInit {

  constructor(public catalogoService: CatalogoService) { }

  onClick(form){
		this.catalogoService.postDados(form)
    window.location.reload();
	}

  ngOnInit() {
  }

}
