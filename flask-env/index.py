from pymongo import MongoClient
from flask import Flask, request, jsonify, render_template
from flask_cors import CORS
from bson import json_util
import json

client = MongoClient('localhost', 27017)

db = client.teste_db

registro1 = {'nome': 'Lucas', 'email': 'lucas@gmail.com', 'telefone': '11 99389-3244'}
registro2 = {'nome': 'Lara', 'email': 'lara@gmail.com', 'telefone': '11 99333-3556'}

catalogos = db.catalogos
catalogos.insert_one(registro1).inserted_id
catalogos.insert_one(registro2).inserted_id

app = Flask(__name__)
CORS(app)

@app.route("/")
def home():
	return render_template('http://localhost:4200/')

@app.route("/get_records", methods=['GET'])
def see():
	return jsonify([json.loads(json.dumps(catalogo, indent=4, default=json_util.default))
                for catalogo in catalogos.find()])

@app.route("/insert_record", methods=['POST'])
def insert():
	nome = request.json['nome'].encode('ascii', 'ignore')
	email = request.json['email'].encode('ascii', 'ignore')
	telefone = request.json['telefone'].encode('ascii', 'ignore')
	registroNovo = {'nome': nome, 'email': email, 'telefone': telefone}
	catalogos.insert_one(registroNovo).inserted_id
	return "Info Posted"

if (__name__ == "__main__"):
	app.run()
